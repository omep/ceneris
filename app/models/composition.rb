class Composition < ActiveRecord::Base
  belongs_to :kit
  belongs_to :element
  validates :cantidad, presence: true
  validates :kit, presence: true
  validates :element, presence: true
end
