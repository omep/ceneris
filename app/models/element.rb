class Element < ActiveRecord::Base
	validates :codigo, presence: true
	validates :descripcion, presence: true
	validates :marca, presence: true
	validates :modelo, presence: true
	validates :stock, presence: true

end
