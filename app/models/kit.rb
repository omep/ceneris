class Kit < ActiveRecord::Base
	validates :codigo, presence: true
	validates :kit, presence: true
	validates :marca, presence: true
	validates :stock, presence: true
end
