json.extract! element, :id, :codigo, :descripcion, :marca, :modelo, :stock, :created_at, :updated_at
json.url element_url(element, format: :json)