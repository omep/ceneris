json.extract! composition, :id, :cantidad, :kit_id, :element_id, :created_at, :updated_at
json.url composition_url(composition, format: :json)