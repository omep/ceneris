json.extract! kit, :id, :codigo, :kit, :marca, :stock, :created_at, :updated_at
json.url kit_url(kit, format: :json)