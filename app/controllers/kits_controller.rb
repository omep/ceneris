class KitsController < ApplicationController
  before_action :set_kit, only: [:show, :edit, :update, :destroy]
  # GET /kits
  # GET /kits.json
  def index
    @kits = Kit.all.paginate(page: params[:page],per_page: 10)
    @index_text = 'Lista de kits'
    @menu_kits = true
    
  end

  # GET /kits/1
  # GET /kits/1.json
  def show
    @index_text = 'Ver kit'
    @menu_kits = true
    
  end

  # GET /kits/new
  def new
    @index_text = 'Nuevo kit'
    @kit = Kit.new
    @menu_kits = true
    
  end

  # GET /kits/1/edit
  def edit
    @index_text = 'Editar kit'
    @menu_kits = true
    
  end

  def generar
    @index_text = 'Generar Kits'
    @menu_generar = true
    @view_block_first = 'none'
    @view_block_second = 'none'
    @var_count = 0
    @kit_id = -1
    if request.post?
      if kit_generar
        @var_count = kit_generar[:cantidad].to_i
        @kit_id = kit_generar[:id].to_i
        result = validar_cantidad_kit(kit_generar[:id].to_i,kit_generar[:cantidad].to_i)
        if result[0].size <=0
          @message = 'Uff! menos mal logramos formar los kits que necesitas.'
          @color = 'success'
          @size = 12
          @view_block_first = 'block'
        else
          @message = ':(! lo sentimos, solo hemos podido formar '+result[1].to_s+' kits. Si nos consigues estos elementos te aseguro q hacemos lo q quieras ;).'
          @color = 'danger'
          @size = 6
          @view_block_first = 'block'
          @view_block_second = 'block'
          @elements = result[0]
          ReportMailer.send_items(@elements,(kit_generar[:cantidad].to_i - result[1]),kit_generar[:id].to_i,result[1]).deliver

        end
      end
      
    end
  end


  def despiece_save
    @index_text = 'Despiece de Kits'
    @menu_despiece = true
    @message = 'none'
    @view_block_first = 'none'
    @view_block_second = 'none'
    if request.post?
      #if despiece_save_param
      kits = request.params['kits']
      kits = kits.split(' ')
      for kit in 0...kits.size
        if kit % 2 == 0
          #se obtiene el kit que despiesaremos
          kit_obj =  Kit.find_by(codigo: kits[kit])
          kit_obj.stock = kit_obj.stock - kits[(kit+1)].to_i
          kit_obj.save
          #se obtiene la composicion para aumentar la cantidad de elementos 
          composiciones =  Composition.where('kit_id = ?',kit_obj.id )
          for composicion in composiciones
            #se calcula la cantidad de elementos q sumaremos a la parte individual
            cantidad =  kits[kit+1].to_i * composicion.cantidad
            #se suma la nueva cantidad al elemento
            element =  composicion.element
            element.stock +=  cantidad
            element.save
          end
        end
      end
      #end
    end
    render :despiece
  end
  
  def despiece
    @index_text = 'Despiece de Kits'
    @menu_despiece = true
    @message = 'none'
    @view_block_first = 'none'
    @view_block_second = 'none'
    @color = 'default'
    @var_count = 0
    @element_id = -1
    if request.post?
      if kit_despiece
        @var_count = kit_despiece[:stock]
        @element_id = kit_despiece[:id].to_i
        elements = Element.find_by(id: kit_despiece[:id])
        if elements.stock >= kit_despiece[:stock].to_i
          @message = 'No es necesario descomponer kits, si cuentas con el stock necesario.'
          @view_block_first = 'block'
          @color = 'success'
          #se cuenta con el stock necesario en individuales 
          
        elsif verificar_cantidad(elements,(kit_despiece[:stock].to_i - elements.stock))
          @kits = kits_descomponer(elements,(kit_despiece[:stock].to_i - elements.stock))
          @message = 'Hicimos todo lo que nos pediste, te recomendamos descomponer estos kits: '
          @view_block_second = 'block'
          
          #retorna la cantidad necesaria de kits a descomponer 
        else
          @view_block_first = 'block'
          @color = 'danger'
          @message = 'Hicimos todo lo q pudimos, pero no tenemos stock necesario.'
          #no cuenta con el stock total necesario
        end
      end
    end
  end

  def validar_cantidad_kit(kit,cantidad)
    composiciones =  Composition.where("kit_id = ?", kit)
    minimo = cantidad

    array = []
    for composicion in composiciones
      cantidad_necesaria = composicion.cantidad * cantidad
      if cantidad_necesaria  > composicion.element.stock
        disponible = composicion.element.stock.divmod(composicion.cantidad)
        if minimo > disponible[0] 
          minimo = disponible[0]
        end
        tem = []
        tem[0] = composicion.element
        tem[1] = cantidad_necesaria - composicion.element.stock
        array.push(tem)
      end
    end
    #guardamos los nuevos kits
    save_new_kits(kit,minimo)

    array_final = []
    array_final[0] = array
    array_final[1] = minimo 
    return array_final
    
  end

  def save_new_kits(kit,cantidad)
    kit = Kit.find_by(id: kit)
    kit.stock = kit.stock + cantidad
    kit.save
    composiciones =  Composition.where("kit_id = ?", kit)
    for composicion in composiciones
      cantidad_restar = composicion.cantidad * cantidad
      elemento = composicion.element
      elemento.stock = elemento.stock - cantidad_restar
      elemento.save
    end
  end

  def kits_descomponer(elements,cantidad)
    #obtiene las composiciones
    composition = Composition.where("element_id = ?", elements.id)
    contador = 0
    array = []
    while contador < cantidad
      #ordena la composicion con el metodo de la burbuja
      composicion = ordenar_composicion(composition)
      #se valida si hay stock en el kit
      if composicion[0].kit.stock > 0
        #suma las cantidades para poder validar el limite
        contador += composicion[0].cantidad
        #reduce temporalmente el stock del kit
        composicion[0].kit.stock -= 1 
        
        #agrupamos por elementos
        indice = buscar(array,composicion[0].kit.codigo)
        if indice >= 0
          tem1 = array[indice] 
          tem1[1] += 1
        else
          tem = []
          tem[0] = composicion[0].kit.codigo
          tem[1] = 1
          array.push(tem)
        end
        
      end
    end
    return array
    
  end

  def buscar(array,codigo)
    #busca un elemento en un array y retorna su indice
    indice = 0
    for x in array
      if x[0] == codigo
        return indice
      end
      indice += 1
    end
    return -1
  end

  def verificar_cantidad(elements,cantidad)
    composiciones = Composition.where("element_id = ?", elements.id)
    contador = 0
    #sumamos los elementos q componen los kits
    for composicion in composiciones
      contador +=  composicion.kit.stock * composicion.cantidad
    end
   
    #verificar si tenemos la cantidad necesaria 
    if contador < cantidad
      return false
    end
    return true

  end

  def ordenar_composicion(composiciones)
    #ordena descendientemente por el metodo de la burbuja 
    cambiar = true
    while (cambiar==true) do
      cambiar=false
      for i in 0..composiciones.size-2
        if (composiciones[i].kit.stock < composiciones[i+1].kit.stock)
          temp = composiciones[i]
          composiciones[i] = composiciones[i+1]
          composiciones[i+1] = temp
          cambiar=true
        end
      end
    end
    return composiciones

  end

  # POST /kits
  # POST /kits.json
  def create
    @kit = Kit.new(kit_params)
    @menu_kits = true

    respond_to do |format|
      if @kit.save
        format.html { redirect_to @kit, notice: 'Kit creado exitosamente.' }
        format.json { render :show, status: :created, location: @kit }
      else
        format.html { render :new }
        format.json { render json: @kit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kits/1
  # PATCH/PUT /kits/1.json
  def update
    @menu_kits = true
    respond_to do |format|
      if @kit.update(kit_params)
        format.html { redirect_to @kit, notice: 'Kit actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @kit }
      else
        format.html { render :edit}
        format.json { render json: @kit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kits/1
  # DELETE /kits/1.json
  def destroy
    @kit.destroy
    @menu_kits = true
    respond_to do |format|
      format.html { redirect_to kits_url, notice: 'Kit eliminado existosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kit
      @kit = Kit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kit_params
      params.require(:kit).permit(:codigo, :kit, :marca, :stock)
    end

    def kit_despiece
       params.require(:element).permit(:id, :stock)
    end

    def kit_generar
       params.require(:kit).permit(:id, :cantidad)
    end

    # def despiece_save_param
    #    params.require(:kit).permit(:array)
    # end

    def send_mail_params
      params.require(:mails).permit(:body)
    end
end
