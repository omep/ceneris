class ElementsController < ApplicationController
  before_action :set_element, only: [:show, :edit, :update, :destroy]
  # GET /elements
  # GET /elements.json
  def index
    @elements = Element.all.paginate(page: params[:page],per_page: 10)
    @index_text = 'Lista de elementos'
    @menu_elements = true
    
  end

  # GET /elements/1
  # GET /elements/1.json
  def show
    @index_text = 'Ver elemento'
    @menu_elements = true
    
  end

  # GET /elements/new
  def new
    @index_text = 'Nuevo elemento'
    @menu_elements = true
    @element = Element.new
    
  end

  # GET /elements/1/edit
  def edit
    @index_text = 'Editar elemento'
    @menu_elements = true
    
  end

  def stock
    @index_text = 'Stock de elementos totales'
    @menu_elements_stock = true
    valid = false
    if request.params['codigo']
      if request.params['codigo'].size > 0
        valid = true
      end
    end

    if valid
      @elements = Element.where('codigo = ? ', request.params['codigo'].upcase).order(:stock).paginate(page: params[:page],per_page: 10)
    else
      @elements = Element.order(:stock).paginate(page: params[:page],per_page: 10)
    end
    for element in  @elements
      contador = 0
      compositions = Composition.where('element_id = ?', element.id)
      for  composition in compositions
        contador = contador + (composition.cantidad *  composition.kit.stock)
      end
      element.stock = contador 
    end
    
  end

  # POST /elements
  # POST /elements.json
  def create
    @element = Element.new(element_params)
    @menu_elements = true
    respond_to do |format|
      if @element.save
        format.html { redirect_to @element, notice: 'Elemento creado exitosamente.' }
        format.json { render :show, status: :created, location: @element }
      else
        format.html { render :new }
        format.json { render json: @element.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /elements/1
  # PATCH/PUT /elements/1.json
  def update
    @menu_elements = true
    respond_to do |format|
      if @element.update(element_params)
        format.html { redirect_to @element, notice: 'Elemento creado exitosamente.' }
        format.json { render :show, status: :ok, location: @element }
      else
        format.html { render :edit}
        format.json { render json: @element.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /elements/1
  # DELETE /elements/1.json
  def destroy
    @element.destroy
    @menu_elements = true
    respond_to do |format|
      format.html { redirect_to elements_url, notice: 'Elemento eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_element
      @element = Element.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def element_params
      params.require(:element).permit(:codigo, :descripcion, :marca, :modelo, :stock)
    end

    def element_search_params
      params.require(:element).permit(:codigo)
    end
end
