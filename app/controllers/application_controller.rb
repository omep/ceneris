class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  layout :layout_ceneri
  before_action :authenticate_user!
  protect_from_forgery with: :exception

  protected

  def layout_ceneri
    if devise_controller?
      "login"
    else
      "application"
    end
  end

end
