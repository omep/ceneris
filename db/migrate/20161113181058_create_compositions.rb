class CreateCompositions < ActiveRecord::Migration
  def change
    create_table :compositions do |t|
      t.integer :cantidad
      t.references :kit, index: true, foreign_key: true
      t.references :element, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
