class CreateKits < ActiveRecord::Migration
  def change
    create_table :kits do |t|
      t.string :codigo
      t.string :kit
      t.string :marca
      t.integer :stock

      t.timestamps null: false
    end
  end
end
