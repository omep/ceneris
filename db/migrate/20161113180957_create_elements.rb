class CreateElements < ActiveRecord::Migration
  def change
    create_table :elements do |t|
      t.string :codigo
      t.string :descripcion
      t.string :marca
      t.string :modelo
      t.integer :stock

      t.timestamps null: false
    end
  end
end
